<?php declare(strict_types=1);
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\MenuBundle\EventDispatcher;


final class MenuEvents
{
    private const CREATED = 'symfony_bro.menu.%s_created';

    public static function created(string $name): string
    {
        return \sprintf(self::CREATED, $name);
    }
}