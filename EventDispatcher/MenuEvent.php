<?php declare(strict_types=1);
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\MenuBundle\EventDispatcher;


use Knp\Menu\ItemInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

final class MenuEvent extends Event
{
    /**
     * @var ItemInterface
     */
    private $menu;

    /**
     * @var Request
     */
    private $request;

    public function __construct(ItemInterface $menu, Request $request)
    {
        $this->menu = $menu;
        $this->request = $request;
    }

    /**
     * @return ItemInterface
     */
    public function getMenu(): ItemInterface
    {
        return $this->menu;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }
}