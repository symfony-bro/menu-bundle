<?php declare(strict_types=1);
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\MenuBundle\Tests\Model;

use Knp\Menu\MenuItem;
use SymfonyBro\MenuBundle\Model\AbstractMenuBuilder;

class StubMenuBuilder extends AbstractMenuBuilder
{
    public function buildMenu(): void
    {
        $item = new MenuItem('test_item', $this->getFactory());
        $this->dispatchMenuEvent($item, 'test');
    }
}