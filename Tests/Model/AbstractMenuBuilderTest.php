<?php declare(strict_types=1);

namespace SymfonyBro\MenuBundle\Tests\Model;

use Knp\Menu\MenuFactory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use SymfonyBro\MenuBundle\EventDispatcher\MenuEvent;

/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

class AbstractMenuBuilderTest extends TestCase
{
    public function testEventFlow(): void
    {
        $factory = new MenuFactory();
        /** @var EventDispatcherInterface|MockObject $dispatcher */
        $dispatcher = $this->getMockBuilder(EventDispatcherInterface::class)
            ->setMethods(['dispatch'])
            ->getMockForAbstractClass()
        ;

        $dispatcher->expects($this->once())
            ->method('dispatch')
            ->with(
                $this->equalTo('symfony_bro.menu.test_created'),
                $this->isInstanceOf(MenuEvent::class)
            )
        ;
        $stack = new RequestStack();
        $stack->push(new Request());

        $builder = new StubMenuBuilder($factory, $dispatcher, $stack);
        $builder->buildMenu();
    }
}
