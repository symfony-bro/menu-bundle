<?php declare(strict_types=1);
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\MenuBundle\Model;


use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use SymfonyBro\MenuBundle\EventDispatcher\MenuEvent;
use SymfonyBro\MenuBundle\EventDispatcher\MenuEvents;

abstract class AbstractMenuBuilder
{
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(FactoryInterface $factory, EventDispatcherInterface $eventDispatcher, RequestStack $requestStack)
    {
        $this->factory = $factory;
        $this->eventDispatcher = $eventDispatcher;
        $this->requestStack = $requestStack;
    }

    protected function dispatchMenuEvent(ItemInterface $menu, string $name): void
    {
        $this->eventDispatcher
            ->dispatch(
                MenuEvents::created($name),
                new MenuEvent($menu, $this->requestStack->getCurrentRequest())
            );
    }

    protected function getFactory(): FactoryInterface
    {
        return $this->factory;
    }
}